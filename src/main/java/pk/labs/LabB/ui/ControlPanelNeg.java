/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabB.ui;

import org.springframework.aop.framework.AopContext;
import pk.labs.LabB.Contracts.ControlPanel;

/**
 *
 * @author Mateusz
 */
public class ControlPanelNeg implements Negativeable{

    public ControlPanelNeg() {
        this.utils = new Utils();
    }
        
    Utils utils;

   
    @Override
    public void negative() {
        this.utils.negateComponent(((ControlPanel) AopContext.currentProxy()).getPanel());
    }
    
}
