package pk.labs.LabB;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "pk.labs.LabB.DisplayImpl";
    public static String controlPanelImplClassName = "pk.labs.LabB.ControlPanelImpl";

    public static String mainComponentSpecClassName = "pk.labs.LabB.Contracts.Main";
    public static String mainComponentImplClassName = "pk.labs.LabB.MainBean";
    public static String mainComponentBeanName = "mainBean";
    // endregion

    // region P2
    public static String mainComponentMethodName = "parzenie";
    public static Object[] mainComponentMethodExampleParams = new Object[] { };
    // endregion

    // region P3
    public static String loggerAspectBeanName = "loggerImpl";
    // endregion
}
